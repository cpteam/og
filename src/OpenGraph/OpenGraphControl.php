<?php

namespace CPTeam\Nette\Extensions\OpenGraph;

use Nette\Application\UI\Control;

/**
 * Class OpenGraphControl
 *
 * @package Klima\OpenGraph\Nette
 */
class OpenGraphControl extends Control
{
	/**
	 * @var array
	 */
	private $og = [];
	
	public function render()
	{
		foreach ($this->og as $key => $value) {
			echo "<meta property=\"og:$key\" content=\"$value\">";
		}
	}
	
	/**
	 * @param $value
	 */
	public function setTitle($value)
	{
		$this->setOg("title", $value);
	}
	
	/**
	 * @param $value
	 */
	public function setDescription($value)
	{
		$this->setOg("description", $value);
	}
	
	/**
	 * @param $name
	 * @param $value
	 */
	public function setOg($name, $value)
	{
		$this->og[$name] = $value;
	}
	
	/**
	 * @param $name
	 *
	 * @return array
	 * @throws OgNotExistsException
	 */
	public function getOg($name)
	{
		if (isset($this->og[$name])) {
			return $this->og;
		}
		
		throw new OgNotExistsException();
	}
}

class OpenGraphFactory
{
	
	private $config;
	
	public function __construct(array $config)
	{
		$this->config = $config;
	}
	
	/** @return OpenGraphControl */
	public function create()
	{
		$control = new OpenGraphControl();
		
		foreach ($this->config as $key => $value) {
			$control->setOg($key, $value);
		}
		
		return $control;
	}
}

class OpenGraphExtensionException extends \Exception
{
	
}

class OgNotExistsException extends OpenGraphExtensionException
{
	
}

class OgNotSetException extends OpenGraphExtensionException
{
	
}
