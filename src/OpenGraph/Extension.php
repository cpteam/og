<?php

namespace CPTeam\Nette\Extensions\OpenGraph;

use Nette\DI\CompilerExtension;

/**
 * Class Extension
 *
 * @package Webgarden\Reporting\Task\Nette
 */
class Extension extends CompilerExtension
{
	
	private function getDefaultConfig()
	{
		return [
			'type' => null,
			'title' => null,
			'image' => null,
			'description' => null,
		];
	}
	
	public function loadConfiguration()
	{
		$this->config = $this->getConfig($this->getDefaultConfig());
		
		foreach ($this->config as $key => $item) {
			if ($item == null) {
				throw new OgNotSetException("Value for $key is not set");
			}
		}
		
		$builder = $this->getContainerBuilder();
		$builder->addDefinition($this->prefix("factory"))
			->setFactory(OpenGraphFactory::class, [$this->config]);
	}
	
}
